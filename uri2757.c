#include <stdio.h>
int main()
{
    int A = -10001, B = -1, C = -1;

    //-10000 ≤ A ≤ 10000;
    while(A < -10000 || A > 10000)
    {
        printf("Digite um valor valido para A: -10000 ≤ A ≤ 10000: ");
        scanf("%d", &A);
    }

    //0 ≤ B ≤ 99;
    while(B < 0 || B > 99)
    {
        printf("Digite um valor valido para B: 0 ≤ A ≤ 99: ");
        scanf("%d", &B);
    }

    //-10000 ≤ A ≤ 10000;
    while(C < 0 || C > 999)
    {
        printf("Digite um valor valido para C: 0 ≤ A ≤ 999: ");
        scanf("%d", &C);
    }


    printf("A = %d, B = %d, C = %d\n", A, B, C);
    printf("A = %9d, B = %7d, C = %8d\n", A, B, C);
    printf("A = %09d, B = %07d, C = %08d\n", A, B, C);
    printf("A = %-9d, B = %-7d, C = %-8d\n", A, B, C);



    return 0;
}