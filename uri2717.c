#include <stdio.h>

int main()
{
    int N = 0, A, B;

    while(N < 2 || N > 100)
    {
        printf("N: ");
        scanf("%d", &N);
    }

    while(A < 1 || A > 100)
    {
        printf("A: ");
        scanf("%d", &A);
    }

    while(B < 1 || B > 100)
    {
        printf("B: ");
        scanf("%d", &B);
    }

    if(A + B <= N)
    {
        printf("Farei Hoje!\n");
    }
    else
    {
        printf("Farei Amanhã!\n");
    }

    return 0;
}