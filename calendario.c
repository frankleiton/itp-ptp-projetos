#include <stdio.h>

int main()
{
    int janeiro = 31, fevereiro = 28, marco = 31, abril = 30, maio = 31, junho = 30, julho = 31, agosto = 31, setembro = 30, outubro= 31, novembro = 30, dezembro = 31;
    
    printf("\n-------------------JANEIRO-------------------\n");
    printf("D S T Q Q S S\n");
    int cont = 0;
    for(int i = 1; i <= janeiro; i++)
    {  
        if(i%7 == 0)
        {
            if(i < 10)
            {
                printf("0%d \n", i);
            }
            else
            {
                printf("%d \n", i);
            }
            cont = 0;
        }
        else
        {
            if(i < 10)
            {
                printf("0%d ", i);
            }
            else
            {
                printf("%d ", i);
            }
            cont++;
        }
    }

    printf("\n-------------------FEVEREIRO-------------------\n");
    for(int j = 0; j < cont; j++) printf("   ");
    for(int i = 1; i <= fevereiro; i++)
    {  
        if(i%7 == 0)
        {
            if(i < 10)
            {
                printf("0%d \n", i);
            }
            else
            {
                printf("%d \n", i);
            }
            cont = 0;
        }
        else
        {
            if(i < 10)
            {
                printf("0%d ", i);
            }
            else
            {
                printf("%d ", i);
            }
            cont++;
        }
    }


    printf("\n-------------------MARCO-------------------\n");
    for(int i = 1; i <= marco; i++) i%7 == 0 ? printf("%d \n", i): printf("%d ", i);

    printf("\n-------------------ABRIL-------------------\n");
    for(int i = 1; i <= abril; i++) i%7 == 0 ? printf("%d \n", i): printf("%d ", i);

    printf("\n-------------------MAIO-------------------\n");
    for(int i = 1; i <= maio; i++) i%7 == 0 ? printf("%d \n", i): printf("%d ", i);

    printf("\n-------------------JUNHO-------------------\n");
    for(int i = 1; i <= junho; i++) i%7 == 0 ? printf("%d \n", i): printf("%d ", i);

    printf("\n-------------------JULHO-------------------\n");
    for(int i = 1; i <= julho; i++) i%7 == 0 ? printf("%d \n", i): printf("%d ", i);

    printf("\n-------------------AGOSTO-------------------\n");
    for(int i = 1; i <= agosto; i++) i%7 == 0 ? printf("%d \n", i): printf("%d ", i);

    printf("\n-------------------SETEMBRO-------------------\n");
    for(int i = 1; i <= setembro; i++) i%7 == 0 ? printf("%d \n", i): printf("%d ", i);

    printf("\n-------------------OUTUBRO-------------------\n");
    for(int i = 1; i <= outubro; i++) i%7 == 0 ? printf("%d \n", i): printf("%d ", i);

    printf("\n-------------------NOVEMBRO-------------------\n");
    for(int i = 1; i <= novembro; i++) i%7 == 0 ? printf("%d \n", i): printf("%d ", i);

    printf("\n-------------------DEZEMBRO-------------------\n");
    for(int i = 1; i <= dezembro; i++) i%7 == 0 ? printf("%d \n", i): printf("%d ", i);

    
    printf("\n");
    return 0;
}